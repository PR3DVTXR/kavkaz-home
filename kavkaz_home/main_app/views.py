from django.shortcuts import render


def home(request):
    return render(request, 'main_app/index.html')

def home_list(request):
    return render(request, 'main_app/home_list.html')